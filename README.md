# SmartHouse

```
composer require knpuniversity/oauth2-client-bundle
composer require league/oauth2-google
composer require league/oauth2-facebook
composer require encore
yarn add @symfony/webpack-encore --dev
./node_modules/.bin/encore dev
```



Модели устройств, компании и типы устройств может добавлять только юзер с ролью ROLE_ADMIN.

Для быстрого добавления админа с ROLE_ADMIN и ROLE_SUPER_ADMIN добавлены консольные команды

```
app:addSuperAdmin email password name

app:addAdmin email password name
```
