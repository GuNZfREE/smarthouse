<?php


namespace App\Controller;


use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SocialAuthController extends AbstractController
{
    /**
     * @Route("/connect/google", name="connect_google")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectGoogleAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect();
    }

    /**
     * @Route("/connect/facebook", name="connect_facebook")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectFacebookAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook')
            ->redirect();
    }

    /**
     * @Route("/connect/google/check", name="connect_google_check")
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function connectGoogleCheckAction()
    {
        if (!$this->getUser())
        {
            return new JsonResponse(array('status' => false, 'message' => "Пользователь не найден"));
        }
        else
        {
            return $this->redirectToRoute('mainPage');
        }

    }

    /**
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function connectFacebookCheckAction()
    {
        if (!$this->getUser())
        {
            return new JsonResponse(array('status' => false, 'message' => "Пользователь не найден"));
        }
        else
        {
            return $this->redirectToRoute('mainPage');
        }

    }
}