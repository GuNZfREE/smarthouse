<?php

namespace App\Controller;

use App\Entity\House;
use App\Entity\Room;
use App\Entity\User;
use App\Form\AddUserHouseType;
use App\Form\HouseType;
use App\Repository\HouseRepository;
use App\Repository\RoomRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/house")
 */
class HouseController extends AbstractController
{
    /**
     * @Route("/", name="house_index", methods={"GET"})
     * @IsGranted("ROLE_SUPER_ADMIN", statusCode=403, message="Здесь ничего нет, уходите")
     */
    public function index(HouseRepository $houseRepository): Response
    {
        return $this->render('house/index.html.twig', [
            'houses' => $houseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/rooms", name="house_roomsByHouse", methods={"GET"})
     */
    public function roomsByHouse(House $house): Response
    {
        $this->denyAccessUnlessGranted('view_house', $house,
            'Просматривать дом могут только пользователи этого дома');
        return $this->render('room/roomsByHouse.html.twig', [
            'house' => $house,
        ]);
    }

    /**
     * @Route("/{idH}/rooms/{id}/devices", name="house_devicesByRooms", methods={"GET"})
     */
    public function devicesByRooms(Room $room): Response
    {
        $this->denyAccessUnlessGranted('view_room', $room,
            'Просматривать дом могут только пользователи этого дома');
        return $this->render('user_device/devicesByRoom.html.twig', [
            'room' => $room
        ]);
    }

    /**
     * @Route("/new", name="house_new", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request): Response
    {
        $house = new House();
        $form = $this->createForm(HouseType::class, $house);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $house->addUser($this->getUser());
            $entityManager->persist($house);
            $entityManager->flush();

            return $this->redirectToRoute('profile_house_list', [
                'id' => $this->getUser()->getID()
            ]);
        }

        return $this->render('house/new.html.twig', [
            'house' => $house,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="house_show", methods={"GET"})
     */
    public function show(House $house): Response
    {
        $this->denyAccessUnlessGranted('view_house', $house,
            'Просматривать дом могут только пользователи этого дома');
        return $this->render('house/show.html.twig', [
            'house' => $house,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="house_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, House $house): Response
    {
        $this->denyAccessUnlessGranted('edit_house', $house,
            'Изменять дом может только владелец этого дома');
        $form = $this->createForm(HouseType::class, $house);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profile_house_list', [
                'id' => $this->getUser()->getID()
            ]);
        }

        return $this->render('house/edit.html.twig', [
            'house' => $house,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="house_delete", methods={"DELETE"})
     */
    public function delete(Request $request, House $house): Response
    {
        $this->denyAccessUnlessGranted('remove_house', $house,
            'Удалять дом может только владелец этого дома');
        if ($this->isCsrfTokenValid('delete'.$house->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($house);
            $entityManager->flush();
        }

        return $this->redirectToRoute('house_index');
    }
}
