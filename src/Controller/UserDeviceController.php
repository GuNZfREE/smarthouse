<?php

namespace App\Controller;

use App\Entity\UserDevice;
use App\Form\UserDeviceType;
use App\Repository\UserDeviceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/device")
 */
class UserDeviceController extends AbstractController
{
    /**
     * @Route("/", name="user_device_index", methods={"GET"})
     * @IsGranted("ROLE_SUPER_ADMIN", statusCode=404, message="Здесь никого нет, уходите")
     */
    public function index(UserDeviceRepository $userDeviceRepository): Response
    {
        return $this->render('user_device/index.html.twig', [
            'user_devices' => $userDeviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_device_new", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request): Response
    {
        $userDevice = new UserDevice();
        $form = $this->createForm(UserDeviceType::class, $userDevice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userDevice);
            $entityManager->flush();

            return $this->redirectToRoute('mainPage');
        }

        return $this->render('user_device/new.html.twig', [
            'user_device' => $userDevice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_device_show", methods={"GET"})
     */
    public function show(UserDevice $userDevice): Response
    {
        $this->denyAccessUnlessGranted('view_userdevice', $userDevice,
            'Просматривать устройство могут только пользователи этого дома');
        return $this->render('user_device/show.html.twig', [
            'user_device' => $userDevice,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_device_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserDevice $userDevice): Response
    {
        $this->denyAccessUnlessGranted('edit_userdevice', $userDevice,
            'Изменять устройство могут только пользователи этого дома');
        $form = $this->createForm(UserDeviceType::class, $userDevice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mainPage');
        }

        return $this->render('user_device/edit.html.twig', [
            'user_device' => $userDevice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/changeSwitch", name="user_device_changeSwitch", methods={"POST"})
     */
    public function changeSwitch(UserDevice $userDevice)
    {
        $this->denyAccessUnlessGranted('edit_userdevice', $userDevice,
            'Изменять устройство могут только пользователи этого дома');
        if ($userDevice->getSwitch())
            $userDevice->setSwitch(false);
        else
            $userDevice->setSwitch(true);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($userDevice);
        $entityManager->flush();
        return new JsonResponse(['switch' => $userDevice->getSwitch()]);
    }

    /**
     * @Route("/{id}", name="user_device_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UserDevice $userDevice): Response
    {
        $this->denyAccessUnlessGranted('remove_userdevice', $userDevice, 'Удалять устройство может только владелец дома');
        if ($this->isCsrfTokenValid('delete'.$userDevice->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userDevice);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_device_index');
    }
}
