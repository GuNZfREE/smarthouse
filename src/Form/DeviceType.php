<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Device;
use App\Entity\TypeDevice;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название'
            ])
            ->add('company', EntityType::class, [
                'label' => 'Компания',
                'class' => Company::class,
                'placeholder' => 'Выбрать компанию'
            ])
            ->add('typeDevice', EntityType::class, [
                'label' => 'Тип устройства',
                'class' => TypeDevice::class,
                'placeholder' => 'Выбрать тип'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Краткое описание',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Device::class,
        ]);
    }
}
