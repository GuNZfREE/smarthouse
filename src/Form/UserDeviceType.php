<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Device;
use App\Entity\House;
use App\Entity\Room;
use App\Entity\UserDevice;
use App\Repository\HouseRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserDeviceType extends AbstractType
{
    private $houseRepository;
    private $user;

    public function __construct(HouseRepository $houseRepository, TokenStorageInterface $token)
    {
        $this->houseRepository = $houseRepository;
        $this->user = $token->getToken()->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', EntityType::class, [
                'label' => 'Производитель',
                'class' => Company::class,
                'placeholder' => '',
            ]);

        $formModifierDevice = function (FormInterface $form, Company $company = null) {
            $device = null === $company ? [] : $company->getDevice();

            $form->add('device', EntityType::class, [
                'label' => 'Выберите модель устройства',
                'class' => Device::class,
                'placeholder' => '',
                'choices' => $device
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifierDevice) {
                $data = $event->getData();

                $formModifierDevice($event->getForm(), $data->getCompany());
            }
        );

        $builder->get('company')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifierDevice) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $company = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifierDevice($event->getForm()->getParent(), $company);
            }
        );

        $builder
            ->add('name', TextType::class, [
                'label' => 'Назовите устройство'])
            ->add('switch', ChoiceType::class, [
                'label' => 'Состояние:',
                'choices' => [
                    'Включить' => 1,
                    'Выключить' => 0
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Кратко опишите его',
            ])
            ->add('house', EntityType::class, [
                'label' => 'К какому дому привязать устройство',
                'class' => House::class,
                'placeholder' => '',
                'query_builder' => function (HouseRepository $house) {
                return $house->createQueryBuilder('u')
                    ->where('u.author = :id')
                    ->setParameter('id', $this->user->getId());
                }
            ]);

        $formModifierRoom = function (FormInterface $form, House $house = null) {
            $rooms = null === $house ? [] : $house->getRooms();

            $form->add('room', EntityType::class, [
                'label' => 'К какой комнате привязать устройство?',
                'class' => Room::class,
                'placeholder' => '',
                'choices' => $rooms
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifierRoom) {
                $data = $event->getData();

                $formModifierRoom($event->getForm(), $data->getHouse());
            }
        );

        $builder->get('house')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifierRoom) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $house = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifierRoom($event->getForm()->getParent(), $house);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserDevice::class,
        ]);
    }
}
