<?php


namespace App\Security\Voter;


use App\Entity\House;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class HouseVoter extends Voter
{
    const VIEW = 'view_house';
    const EDIT = 'edit_house';
    const DELETE = 'remove_house';

    private $security;
    private $decisionManager;
    private const ATTRIBUTES = [
        self::VIEW,
        self::EDIT,
        self::DELETE,
    ];

    public function __construct(Security $security, AccessDecisionManagerInterface $decisionManager)
    {
        $this->security = $security;
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof House
            && in_array($attribute, self::ATTRIBUTES);

    }

    /**
     * @param string $attribute
     * @param User $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        /** @var House $post */
        $post = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($post, $user);
            case self::EDIT:
                return $this->canEdit($post, $user);
            case self::DELETE:
                return $this->canDelete($post, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(House $post, User $user)
    {
        if ($this->canEdit($post, $user)) {
            return true;
        }

        return in_array($user, $post->getUsers()->toArray());
    }

    private function canEdit(House $post, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $post->getAuthor();
    }

    private function canDelete(House $post, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $post->getAuthor();
    }
}