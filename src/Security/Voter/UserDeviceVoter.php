<?php


namespace App\Security\Voter;


use App\Entity\Device;
use App\Entity\User;
use App\Entity\UserDevice;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class UserDeviceVoter extends Voter
{
    const VIEW = 'view_userdevice';
    const EDIT = 'edit_userdevice';
    const DELETE = 'remove_userdevice';

    private $security;
    private const ATTRIBUTES = [
        self::VIEW,
        self::EDIT,
        self::DELETE,
    ];

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof UserDevice
            && in_array($attribute, self::ATTRIBUTES);

    }

    /**
     * @param string $attribute
     * @param User $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
            return true;
        }

        /** @var UserDevice $post */
        $post = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($post, $user);
            case self::EDIT:
                return $this->canEdit($post, $user);
            case self::DELETE:
                return $this->canDelete($post, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(UserDevice $post, User $user)
    {
        return in_array($user, $post->getHouse()->getUsers()->toArray());
    }

    private function canEdit(UserDevice $post, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return in_array($user, $post->getHouse()->getUsers()->toArray());
    }

    private function canDelete(UserDevice $post, User $user)
    {
        return $user === $post->getAuthor();
    }
}