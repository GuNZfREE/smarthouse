<?php

namespace App\Entity;

use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use App\Helper\StatusInterface;
use App\Helper\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeDeviceRepository")
 * @UniqueEntity(fields="name", message="Такой тип устройств уже есть")
 * @ORM\HasLifecycleCallbacks()
 */
class TypeDevice implements AuthorInterface, StatusInterface
{
    use StatusTrait;
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Device", mappedBy="typeDevice", cascade={"persist"})
     */
    private $device;

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->name;
    }

    public function __construct()
    {
        $this->setCreatedAt(0);
        $this->setStatus(1);
        $this->device = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevice(): Collection
    {
        return $this->device;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->device->contains($device)) {
            $this->device[] = $device;
            $device->setTypeDevice($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->device->contains($device)) {
            $this->device->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getTypeDevice() === $this) {
                $device->setTypeDevice(null);
            }
        }

        return $this;
    }

}
