<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190829113214 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(4096) NOT NULL, created_at INT DEFAULT 0 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_4FBF094FF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, type_device_id INT NOT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(4096) DEFAULT NULL, created_at INT DEFAULT 0 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_92FB68E979B1AD6 (company_id), INDEX IDX_92FB68E206286C4 (type_device_id), INDEX IDX_92FB68EF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE house (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at INT DEFAULT 0 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_67D5399DF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, house_id INT DEFAULT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at INT DEFAULT 0 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_729F519B6BB74515 (house_id), INDEX IDX_729F519BF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_device (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, status INT DEFAULT 1 NOT NULL, created_at INT DEFAULT 0 NOT NULL, UNIQUE INDEX UNIQ_2AF8DDB65E237E06 (name), INDEX IDX_2AF8DDB6F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT NULL, google_id VARCHAR(255) DEFAULT NULL, facebook_id VARCHAR(255) DEFAULT NULL, created_at INT NOT NULL, status INT DEFAULT 1 NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_house (user_id INT NOT NULL, house_id INT NOT NULL, INDEX IDX_8517C2C5A76ED395 (user_id), INDEX IDX_8517C2C56BB74515 (house_id), PRIMARY KEY(user_id, house_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_device (id INT AUTO_INCREMENT NOT NULL, device_id INT NOT NULL, house_id INT NOT NULL, room_id INT DEFAULT NULL, company_id INT NOT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, switch TINYINT(1) DEFAULT NULL, description VARCHAR(4096) DEFAULT NULL, created_at INT DEFAULT 0 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_6C7DADB394A4C7D4 (device_id), INDEX IDX_6C7DADB36BB74515 (house_id), INDEX IDX_6C7DADB354177093 (room_id), INDEX IDX_6C7DADB3979B1AD6 (company_id), INDEX IDX_6C7DADB3F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68E206286C4 FOREIGN KEY (type_device_id) REFERENCES type_device (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE house ADD CONSTRAINT FK_67D5399DF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B6BB74515 FOREIGN KEY (house_id) REFERENCES house (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE type_device ADD CONSTRAINT FK_2AF8DDB6F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_house ADD CONSTRAINT FK_8517C2C5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_house ADD CONSTRAINT FK_8517C2C56BB74515 FOREIGN KEY (house_id) REFERENCES house (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB394A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB36BB74515 FOREIGN KEY (house_id) REFERENCES house (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB354177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB3979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE user_device ADD CONSTRAINT FK_6C7DADB3F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68E979B1AD6');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB3979B1AD6');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB394A4C7D4');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B6BB74515');
        $this->addSql('ALTER TABLE user_house DROP FOREIGN KEY FK_8517C2C56BB74515');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB36BB74515');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB354177093');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68E206286C4');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF675F31B');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EF675F31B');
        $this->addSql('ALTER TABLE house DROP FOREIGN KEY FK_67D5399DF675F31B');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519BF675F31B');
        $this->addSql('ALTER TABLE type_device DROP FOREIGN KEY FK_2AF8DDB6F675F31B');
        $this->addSql('ALTER TABLE user_house DROP FOREIGN KEY FK_8517C2C5A76ED395');
        $this->addSql('ALTER TABLE user_device DROP FOREIGN KEY FK_6C7DADB3F675F31B');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE house');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE type_device');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_house');
        $this->addSql('DROP TABLE user_device');
    }
}
