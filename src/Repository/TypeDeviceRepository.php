<?php

namespace App\Repository;

use App\Entity\TypeDevice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeDevice|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeDevice|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeDevice[]    findAll()
 * @method TypeDevice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeDeviceRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeDevice::class);
    }

    // /**
    //  * @return TypeDevice[] Returns an array of TypeDevice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeDevice
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
