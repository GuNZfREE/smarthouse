<?php


namespace App\Repository;


use App\Helper\AbstractStatusClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @return AbstractStatusClass
     * @throws \ReflectionException
     */
    protected function getRepositoryStatusName()
    {
        $function = new \ReflectionClass($this->getClassName());
        $statusClassName = 'App\Helper\Status\\' . $function->getShortName() . "Status";
        return new $statusClassName;
    }

    /**
     * @return array|mixed
     * @throws \ReflectionException
     */
    public function findAll()
    {
        $qb = $this->createQueryBuilder('e');
        $statusClass = $this->getRepositoryStatusName();
        $qb->andWhere($qb->expr()->eq('e.status', ':status'));
        $qb->setParameter(':status', $statusClass::ACTIVE);
        return $qb->getQuery()->getResult();
    }

}