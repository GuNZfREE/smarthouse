<?php

namespace App\Helper;

abstract class AbstractStatusClass
{
    const ACTIVE = 1;
    const ARCHIVE = 11;

    protected static $statusNames = [
        self::ACTIVE => 'Активный',
        self::ARCHIVE => 'Архивный'
    ];

    protected static $statusTypes = [
        self::ACTIVE => 'primary',
        self::ARCHIVE => 'warning'
    ];

    public static function getStatusNames()
    {
        return self::$statusNames;
    }

    public static function getStatusTypes()
    {
        return self::$statusTypes;
    }

    public static function getName($key)
    {
        if (isset(static::$statusNames)) {
            if (array_key_exists($key, static::$statusNames)) {
                return static::$statusNames[$key];
            }
        }

        return '';
    }

    public static function getType($key)
    {
        if (isset(static::$statusTypes)) {
            if (array_key_exists($key, static::$statusTypes)) {
                return static::$statusTypes[$key];
            }
        }

        return '';
    }


}