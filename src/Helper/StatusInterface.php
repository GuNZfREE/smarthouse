<?php


namespace App\Helper;


interface StatusInterface
{
    public function getStatus(): ?int;
    public function setStatus(int $status);
}
