<?php


namespace App\Helper;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait StatusTrait
{
    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false, options={"default": 1})
     */
    protected $status;

    /**
     * @return integer
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param integer $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }


    /**
     * @return AbstractStatusClass
     * @throws \ReflectionException
     */
    protected function getStatusClass()
    {
        $function = new \ReflectionClass($this);
        $statusClassName = 'App\Helper\Status\\' . $function->getShortName() . "Status";
        $statusClass = new $statusClassName;
        return $statusClass;
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public function getStatusName()
    {
        /** @var AbstractStatusClass $statusClass */
        $statusClass = $this->getStatusClass();
        return $statusClass::getName($this->getStatus());
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public function getStatusType()
    {
        return $this->getStatusClass()->getType($this->getStatus());
    }

    /**
     * @return bool
     * @throws \ReflectionException
     */
    public function editAllowed()
    {
        return self::getStatus() == $this->getStatusClass()::ACTIVE;
    }

    /**
     * @return bool
     * @throws \ReflectionException
     */
    public function isArchive()
    {
        return self::getStatus() == $this->getStatusClass()::ARCHIVE;
    }

    /**
     * @return bool
     * @throws \ReflectionException
     */
    public function isActive()
    {
        return self::getStatus() == $this->getStatusClass()::ACTIVE;
    }
}
