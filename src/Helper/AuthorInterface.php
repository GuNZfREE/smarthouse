<?php

namespace App\Helper;


use Symfony\Component\Security\Core\User\UserInterface;

interface AuthorInterface
{
    public function getAuthor(): ?UserInterface;
    public function setAuthor(UserInterface $user);

    public function getCreatedAt(): ?int;
    public function setCreatedAt(int $createdAt);
}
